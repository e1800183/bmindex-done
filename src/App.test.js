import { render, screen } from '@testing-library/react';
import App from "./App";
import { configure, shallow, assert } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

configure({ adapter: new Adapter() });
describe('The this-is-me data")', () => {
  it("There is one image", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find("img").length>0).toBe(true);
  });

  it("There is min two inputs", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find("input").length > 1).toBe(true);
  });
});

